NOTE: Please build this Kanban board from scratch without pre-built components such as 'react-trello'...

1. Create a list of 4 boards with 2 default items in each list.

2. Style boards according to the png provided.

3. Create the functionality to add a task to each list.

4. Create the buttons and functionality to move cards from one list to another.

5. Persist cards in the browser.
